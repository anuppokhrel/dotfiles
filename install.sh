#!/usr/bin/env bash

set -eu -o pipefail

DIR="$(cd "$(dirname "$0")"; pwd)"
echo $DIR

install_tmux_config() {
    echo "Installing config file"
    LOCALREPO=.oh-my-tmux

    if [ ! -d $LOCALREPO ]
    then
        git clone https://github.com/gpakosz/.tmux.git $LOCALREPO
    else
        cd $LOCALREPO
        git pull origin master
        cd ..
    fi
    ln -s -f $PWD/.oh-my-tmux/.tmux.conf -t ~/
}
install_zsh_config() {
    echo "Installing oh-my-zsh"
    ln -s -f $PWD/.zshrc -t ~/
    LOCALREPO=~/.oh-my-zsh
    ORIGPATH=$PWD
    if [ ! -d $LOCALREPO ]
    then
        git clone https://github.com/ohmyzsh/ohmyzsh.git $LOCALREPO
    else
        cd $LOCALREPO
        git pull origin master
        cd ..
    fi
    cd $ORIGPATH
}

install_tmux_config
install_zsh_config

# Copy neovim config
ln -s -f $PWD/nvim ../nvim
nvim +'PlugInstall --sync' +qa
